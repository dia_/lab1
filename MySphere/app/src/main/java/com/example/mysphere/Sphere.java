package com.example.mysphere;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;
/**
 * Класс нахождения координат вершин объекта, отрисовка
 */
public class Sphere {
    /**
     * отрисовка
     */
    public void draw(GL10 gl){
        float[][] a = new float[32][3];
        ByteBuffer b;
        FloatBuffer f;

        b = ByteBuffer.allocateDirect(a.length * a[0].length * 4);
        b.order(ByteOrder.nativeOrder());
        f = b.asFloatBuffer();
        //поодерка массивов вершин, нормалей
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);

        float	cos, sin, angA, angB, r, r1, h, h1;
        float	stp = 30.0f;
        //при вычислении координат вершин, идем с отрицательного направления оси y, увеличивая угол,
        // пока ось не окажется положительной
        for (angA = -90.0f; angA <90.0f; angA += stp) {
            int	n = 0;

            r = (float)Math.cos(angA * Math.PI / 180.0);
            r1 = (float)Math.cos((angA + stp) * Math.PI / 180.0);
            h = (float)Math.sin(angA * Math.PI / 180.0);
            h1 = (float)Math.sin((angA + stp) * Math.PI / 180.0);

            // фиксированное значение y, поворот от 0 до 360 градусов
            for (angB = 0.0f; angB <= 360.0f; angB += stp) {

                cos = (float)Math.cos(angB * Math.PI / 180.0);
                sin = -(float)Math.sin(angB * Math.PI / 180.0);
                a[n][0] = (r1 * cos);
                a[n][1] = (h1);
                a[n][2] = (r1 * sin);
                a[n + 1][0] = (r * cos);
                a[n + 1][1] = (h);
                a[n + 1][2] = (r * sin);
                f.put(a[n]);
                f.put(a[n + 1]);
                n += 2;

                if(n>31){
                    f.position(0);
                    //записываем размер, тип, и сам буфер с точками
                    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, f);
                    //определяет массив нормалей
                    gl.glNormalPointer(GL10.GL_FLOAT, 0, f);
                    //отрисовывает объект
                    gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, n);
                    n = 0;
                    angB -= stp;
                }
            }
            f.position(0);

            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, f);
            gl.glNormalPointer(GL10.GL_FLOAT, 0, f);
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, n);
        }
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);
    }
}


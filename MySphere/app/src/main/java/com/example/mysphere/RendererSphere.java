package com.example.mysphere;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Класс рендера сферы и работа со светом
 */
public class RendererSphere implements GLSurfaceView.Renderer{

    //свет
    private final float[] ambient = { 0.2f, 0.3f, 0.4f, 1.0f };
    private FloatBuffer ambient_f;
    //Еще падающий свет
    private final float[] diffuse = { 0.4f, 0.6f, 0.8f, 1.0f };
    private FloatBuffer diffuse_f;
    // Выделенная область
    private final float[] specular = { 0.2f * 0.4f, 0.2f * 0.6f, 0.2f * 0.8f, 1.0f };
    private FloatBuffer specular_f;
    //создаем объект
    private Sphere mySphere = new Sphere();
    //координаты
    public volatile float X = 10f;
    public volatile float Y = 10f;
    public volatile float Z = 10f;

    /**
     * метод вызывается при создании поверхности
     */
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);
        //черный фон
        gl.glClearColor(0, 0.0f, 0.0f, 0.0f);
        //сглаживание цвета
        gl.glShadeModel(GL10.GL_SMOOTH);
        //параметр задает значение глубины, используемое при очистке буфера глубины
        gl.glClearDepthf(1.0f);
        //включен тест глубины, будет автоматически сохранять значения глубины для всех прошедших
        //тест фрагментов
        gl.glEnable(GL10.GL_DEPTH_TEST);
        //фрагмент походит тест, если его значение глубины меньше или равно хранимому в буфере
        gl.glDepthFunc(GL10.GL_LEQUAL);

        initBuffers();
    }

    /**
     * метод вызывается при изменении размера поверхности
     */
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        //ф-я, чтобы перестроить вывод изображения в окно с такими размерами
        gl.glViewport(0, 0, width, height);

        //задаем матричный режим, GL_PROJECTION - матрица проекций
        gl.glMatrixMode(GL10.GL_PROJECTION);
        //очищаем матрицу
        gl.glLoadIdentity();

        //устанавливаем тип текуцей проекции
        GLU.gluPerspective(gl, 90.0f, (float) width / height, 0.1f, 50.0f);
        //когда проекция определена, устанавливаем
        //в качестве текущей матрицы объектно-видовую матрицу
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //очищаем её
        gl.glLoadIdentity();
        gl.glColor4f(1,0,0,1);
    }

    /**
     * метод вызывается для рисования текущего кадра
     */
    @Override
    public void onDrawFrame(GL10 gl) {
        //очищаем буферы цвета и глубины
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        //сброс просмотра
        gl.glLoadIdentity();
        //включить освещение
        gl.glEnable(GL10.GL_LIGHTING);
        //нулевой источние света
        gl.glEnable(GL10.GL_LIGHT0);

        // свойства задаются для обеих сторон поверхности, цвет отражения фонового материала, свойс-
        //-тво передается через последний параметр
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, ambient_f);
        //цвет рассеянного отражения материала
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, diffuse_f);
        //цвет зеркального отражения материала
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, specular_f);
        // коффициент блеска
        gl.glMaterialf(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, 100.0f);

        //положение источника света
        float[] light_position = {X, Y, Z, 0.0f};
        ByteBuffer mb = ByteBuffer.allocateDirect(light_position.length*4);
        mb.order(ByteOrder.nativeOrder());
        FloatBuffer posf = mb.asFloatBuffer();
        posf.put(light_position);
        posf.position(0);

        //позиция направленного источника света
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, posf);
        //сдвиг
        gl.glTranslatef(0.0f, 0.0f, -3.0f);
        //отрисовка
        mySphere.draw(gl);

    }

    /**
     * создание буферов света
     */
    private void initBuffers() {
        ByteBuffer bufTemp = ByteBuffer.allocateDirect(ambient.length * 4);
        bufTemp.order(ByteOrder.nativeOrder());
        ambient_f = bufTemp.asFloatBuffer();
        ambient_f.put(ambient);
        ambient_f.position(0);

        bufTemp = ByteBuffer.allocateDirect(diffuse.length * 4);
        bufTemp.order(ByteOrder.nativeOrder());
        diffuse_f = bufTemp.asFloatBuffer();
        diffuse_f.put(diffuse);
        diffuse_f.position(0);

        bufTemp = ByteBuffer.allocateDirect(specular.length * 4);
        bufTemp.order(ByteOrder.nativeOrder());
        specular_f = bufTemp.asFloatBuffer();
        specular_f.put(specular);
        specular_f.position(0);
    }
}

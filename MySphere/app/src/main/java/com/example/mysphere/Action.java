package com.example.mysphere;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
/**
 * Класс изменений при нажатях
 */
public class Action extends GLSurfaceView {
    private RendererSphere myRenderer;
    private float mDownX = 0.0f;
    private float mDownY = 0.0f;

    public Action(Context context) {
        super(context);
        myRenderer = new RendererSphere(); //отрисовка сферы
        this.setRenderer(myRenderer);
    }

    /**
     * обработка движений по экрану
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int ac = event.getActionMasked();
        switch (ac) {
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getX();
                mDownY = event.getY();
                return true;
            case MotionEvent.ACTION_UP:
                return true;
            case MotionEvent.ACTION_MOVE:
                float mX = event.getX();
                float mY = event.getY();
                myRenderer.X += (mX-mDownX)/10;
                myRenderer.Y -= (mY-mDownY)/10;
                mDownX = mX;
                mDownY = mY;
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }
}

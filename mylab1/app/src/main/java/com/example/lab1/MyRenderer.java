package com.example.lab1;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
/**
 * Класс создания вершин и отрисовки квадрата
 * @author Дарья Панченко.
 */

public class MyRenderer implements GLSurfaceView.Renderer {
    //массив координат точек вершин
    float []a=new float[]{
            -0.5f, -0.5f, 0.0f,
            -0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f,
            0.5f, 0.5f, 0.0f
    };
    FloatBuffer f;
    ByteBuffer b;
    private float angl = 0.0f;

    //Указываем буфер вершин
    public MyRenderer(){
        b=ByteBuffer.allocateDirect(a.length*4);
        b.order(ByteOrder.nativeOrder());
        f=b.asFloatBuffer();
        f.put(a);
        f.position(0);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

    }

    @Override
    /**
     * метод вызывается для рисования текущего кадра
     */
    public void onDrawFrame(GL10 gl) {
        //ставим цвет фона - чёрный
        gl.glClearColor(0,0,0,0);
        //очищаем буферы цвета и глубины
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT| GL10.GL_DEPTH_BUFFER_BIT);
        //масштабируем
        gl.glScalef(0.5f,0.5f,0.5f);
        //поддержка массивов вершин
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        //цвет объекта - бирюзовый
        gl.glColor4f(0,1,1,1);
        //записываем размер, тип, и сам буыер с точками
        gl.glVertexPointer(3, GL10.GL_FLOAT,0,f);
        //отрисовка с помощью GL_TRIANGLE_STRIP
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0,a.length/3);
        //сброс матрицы
        gl.glLoadIdentity();

        gl.glRotatef(angl, 1.0f, 1.0f, 1.0f);
        angl++;
    }
}

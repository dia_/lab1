package com.example.mycube;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;
/**
 * Класс создания вершин, граней и отрисовки объекта
 * @author Дарья Панченко.
 */
public class Cube {
   //вершины куба
    private float a[] = {
           -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
           -1.0f,  -1.0f, 1.0f,
           1.0f, -1.0f, 1.0f,
           -1.0f, 1.0f, - 1.0f,
           1.0f, 1.0f, - 1.0f,
           -1.0f,  -1.0f,  -1.0f,
           1.0f, -1.0f, -1.0f

    };
    //6 граней куба
    private byte index[] = {
            //ближняя
            1, 3, 0, 0, 3, 2,
            //дальняя
            4, 6, 5, 5, 6, 7,
            //левая
            0, 2, 4, 4, 2, 6,
            //правая
            5, 7, 1, 1, 7, 3,
            //верхняя
            5, 1, 4, 4, 1, 0,
            //нижняя
            6, 2, 7, 7, 2, 3
    };

    private FloatBuffer aB;
    private ByteBuffer iB;

    //Указываем буфер вершин, индексов
    public Cube() {
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(a.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        aB = byteBuf.asFloatBuffer();
        aB.put(a);
        aB.position(0);

        iB = ByteBuffer.allocateDirect(index.length);
        iB.put(index);
        iB.position(0);
    }
    /**
     * метод отрисовки объекта
     */
    public void draw(GL10 gl) {
        gl.glFrontFace(GL10.GL_CW);
        //записываем размер, тип, и сам буыер с точками
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, aB);
        //поддержка массивов вершин
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        //отрисовывает объект по индексам из буфера индексов
        gl.glDrawElements(GL10.GL_TRIANGLES, 36, GL10.GL_UNSIGNED_BYTE, iB);
        //поддержка массивов вершин
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    }

}

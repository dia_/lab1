package com.example.mycube;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
/**
 * Класс рендера куба
 * @author Дарья Панченко.
 */
public class RendererCube implements GLSurfaceView.Renderer {
    //объект
    private Cube myCube = new Cube();
    private float CRotate;

    @Override
    /**
     * метод вызывается при создании поверхности
     */
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //параметр задает значение глубины, используемое при очистке буфера глубины
        gl.glClearDepthf(1.0f);
        //включен тест глубины, будет автоматически сохранять значения глубины для всех прошедших
        //тест фрагментов
        gl.glEnable(GL10.GL_DEPTH_TEST);
        //фрагмент походит тест, если его значение глубины меньше или равно хранимому в буфере
        gl.glDepthFunc(GL10.GL_LEQUAL);
        //подсказки
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,
                GL10.GL_NICEST);
    }

    @Override
    /**
     * метод вызывается при изменении размера поверхности
     */
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        //ф-я, чтобы перестроить вывод изображения в окно с другими размерами
        gl.glViewport(width/2, height/2, width/2, height/2);
        //задаем матричный режим, GL_PROJECTION - матрица проекций
        gl.glMatrixMode(GL10.GL_PROJECTION);
        //очищаем матрицу
        gl.glLoadIdentity();
        //устанавливаем тип текуцей проекции
        GLU.gluPerspective(gl, 45.0f, (float)width / (float)height, 0.1f, 200.0f);
        gl.glViewport(width/2, height/2, width/2, height/2);
        //когда проекция определена, устанавливаем
        //в качестве текущей матрицы объектно-видовую матрицу
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //очищаем её
        gl.glLoadIdentity();
        gl.glColor4f(0,1,1,1);

    }

    @Override
    /**
     * метод вызывается для рисования текущего кадра
     */
    public void onDrawFrame(GL10 gl) {
        //очищаем буферы цвета и глубины
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        //сброс просмотра
        gl.glLoadIdentity();
        //сдвиг
        gl.glTranslatef(0.0f, 0.0f, -10.0f);
        //вращение матрицы
        gl.glRotatef(CRotate, 1.0f, 1.0f, 1.0f);
        //отрисовка
        myCube.draw(gl);

        gl.glLoadIdentity();

        CRotate += 0.10f;
    }
}
